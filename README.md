# Topher Lee
### Hello there 👋

[![](https://vistr.dev/badge?repo=TopherLee513.TopherLee513&corners=square)](https://github.com/TopherLee513/vistr.dev)
[![](https://img.shields.io/badge/-@TopherLee513-%23181717?style=flat-square&logo=github)](https://github.com/TopherLee513)
[![](https://img.shields.io/badge/-@TopherLee513-%231DA1F2?style=flat-square&logo=twitter&logoColor=ffffff)](https://twitter.com/TopherLee513)
[![](https://img.shields.io/badge/-@altered-existence-%23181717?style=flat-square&logo=github)](https://github.com/altered-existence)
[![Discord](https://img.shields.io/discord/356807608256036866.svg?color=7289da&label=Alt-X&logo=discord&style=flat-square)](https://discord.gg/356807608256036866)
[![](https://img.shields.io/badge/-Christopher%20Lee-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/christopher-lee-a423113b/)](https://www.linkedin.com/in/christopher-lee-a423113b/)
<!--
[![Discord](https://img.shields.io/discord/356807608256036866.svg?label=&logo=discord&logoColor=ffffff&color=7389D8&labelColor=6A7EC2)](https://discord.com/channels/356807608256036866)
-->
- 🔭 I’m currently working on ...
  - this
  - Various Godot Game Projects
  - A couple websites/webapps

- 🌱 I’m currently learning ...
  - Web Development Technologies
    - NodeJS
      - Express
      - EJS
      - VueJS
    
    - C#/Mono/.NET
      - Razor/Blazor Framework
      - Godot Engine
      - Unity Engine

  - Server Management
    - Windows Server
    - Ubuntu 16.04+
    - Nginx Web Server
    - Apache 2.0 Web Server
    - NodeJS Web Server
    - MariaDB SQL Database Server

  - Video Game Development
    - Unity Engine
    - Godot Engine
    - BabylonJS

  - Programming Languages
    - C#
    - JS/TS
    - HTML/CSS
    - Markdown :)

- 📫 How to reach me: ...
  - [Discord]()
  - [Twitter](https://twitter.com/TopherLee513)
  - [Facebook](https://www.facebook.com/topher.lee.13)
  - Gmail - [topher.lee.13@gmail.com](topher.lee.13@gmail.com)

- ⚡ Fun fact: ...
  - [topher-lee.nft](https://www.topher-lee-nft.web.app) is hosted on Google's Firebase Service

## Stats

![Topher's Github Stats](https://github-readme-stats.vercel.app/api?username=TopherLee513&show_icons=true&theme=dracula)
![1](https://github-readme-stats.vercel.app/api/top-langs/?username=TopherLee513&theme=blue-green)
<!--
**TopherLee513/TopherLee513** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:


- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 😄 Pronouns: ...

-->
